val commonSettings = Seq(
  version := "1.0",
  scalaVersion := Version.scala
)

lazy val root = (project in file("."))
  .settings(
    name := """paintshop""",
    commonSettings,
    libraryDependencies ++= Dependencies.core,
    dependencyOverrides ++= Dependencies.overrides
  )

mainClass in (Compile, run) := Some("paintshop.Main")