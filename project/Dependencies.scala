import sbt._

object Version {
  val scala = "2.11.8"
  val scalaTest = "2.2.6"
  val scalaXml = "1.0.4"
  val scalaReflect = "2.11.8"
}

object Library {
  val scalaTest = "org.scalatest" %% "scalatest" % Version.scalaTest % "test"
  val betterFiles = "com.github.pathikrit" %% "better-files" % "2.15.0"
}

object Dependencies {

  import Library._

  val core = Seq(
    scalaTest,
    betterFiles
  )

  val overrides = Set(
    "org.scala-lang.modules" %% "scala-xml" % Version.scalaXml,
    "org.scala-lang" % "scala-reflect" % Version.scalaReflect
  )
}