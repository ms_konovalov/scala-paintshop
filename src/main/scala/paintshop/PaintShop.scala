/** Provides classes for solving PaintShop task
  *
  * ==Overview==
  * The main class to use is [[paintshop.PaintShopSolver]], as so
  * {{{
  * scala> val result = paintshop.PaintShopSatisfier().solve(Order(1, OrderItem((1, Matte)))
  * result: paintshop.Success = Success(1 -> Matte)
  * }}}
  */
package paintshop

import better.files.{File, Scanner}
import paintshop.ColorType.Glossy

import scala.annotation.tailrec
import scala.io.StdIn

/** Case class represents whole order (test case) made by all of the customers
  *
  * @param items set of customers' wishes presented via [[Seq]]
  * @param colorsNum amount of colors
  */
case class Order(items: Seq[OrderItem], colorsNum: Int)

/** Factory for [[Order]] objects to simplify syntax */
object Order {

  /** Creates an [[Order]] with a [[Seq]] of [[OrderItem]]s from varargs tuples
    *
    * @param colorsNum amount of colors necessary for filling undecided colors with default walue
    * @param items vararg [[OrderItem]]s
    * @return created object [[Order]] with corresponding [[OrderItem]]s
    */
  def apply(colorsNum: Int, items: OrderItem*) = new Order(items, colorsNum)
}

/** Case class represents Order item made by one customer which consists of several clauses
  *
  * @param clauses set of customers' clauses presented via [[Seq]]
  */
case class OrderItem(clauses: Seq[Clause]) {

  /** Method do detect whether DeviceItem is unary (contains only 1 clause) or not
    *
    * @return true, if order item is unary
    */
  def isUnary: Boolean = clauses.size == 1
}

/** Factory for [[OrderItem]] objects to simplify syntax */
object OrdItem {

  /** Creates an [[OrderItem]] with a [[Seq]] of [[Clause]]s from vararg tuples
    *
    * @param items vararg tuples of types [[Color]] and [[ColorType.Value]]
    * @return
    */
  def apply(items: (Color, ColorType.Value)*) = new OrderItem(items.toSeq.map(x => Clause(x._1, x._2)))
}

/** Case class represents one clause from a customer for i paint color
  *
  * @param color color
  * @param colorType type of color that customer would like to receive
  */
case class Clause(color: Color, colorType: ColorType.Value)

/** Enumeration for types of colors */
object ColorType extends Enumeration {

  val Matte = Value(1)
  val Glossy = Value(0)
}

/** Trait represents the result of the task */
trait Solution

/** Result of the task if it is impossible to satisfy all customers */
case object Impossible extends Solution {

  override def toString = "IMPOSSIBLE"
}

/** Class represents result of the task when it is possible to satisfy all clients
  *
  * @param result [[Map]] with the types of paints for each color, color is a key
  */
case class Success(result: Map[Color, ColorType.Value]) extends Solution {

  /** Helper method to append type of color to the solution
    *
    * @param kv typle of color and its type
    * @return new [[Success]] result with added color
    */
  def +(kv: (Color, ColorType.Value)): Success = Success(this.result + kv)

  /** Helper method to concatenate two solutions. Necessary for filling undecided colors with default walue
    *
    * @param s [[Success]] result to append
    * @return new [[Success]] result
    */
  def ++(s: Success): Success = Success(this.result ++ s.result)

  /** Method to serialize result to a [[String]]. Converts [[ColorType]] to [[Int]] with 'space' delimeters
    *
    * @return string representation of result
    */
  override def toString = result.foldLeft(new Array[Int](result.size))((arr, kv) => {
    arr.update(kv._1 - 1, kv._2.id)
    arr
  }).mkString(" ")
}

/** Factory for [[Success]]  */
object Success {

  /** Creates empty [[Success]] result
    *
    * @return result [[Success]] without any paint types
    */
  def apply(): Success = Success(Map[Color, ColorType.Value]())

  /** Creates a [[Success]] with a [[Map]] from vararg tuples
    *
    * @param kvs vararg tuples
    * @return result [[Success]] with provided types of colors
    */
  def apply(kvs: (Color, ColorType.Value)*): Success = Success(kvs.toMap)

  /** Creates a [[Success]] result of specified size filled with [[ColorType.Glossy]] paints
    *
    * @param colorNum amount of colors
    * @return result [[Success]] filled with [[ColorType.Glossy]] paints
    */
  def allGlossy(colorNum: Int): Success = Success((1 to colorNum).map(i => i -> Glossy).toMap)
}

/** Trait for task solver interface */
trait PaintShopSolver {

  /** Solution method
    *
    * @param order whole order from all customers
    * @return [[Impossible]], if solution doesn't exist, [[Success]] for solution
    */
  def solve(order: Order): Solution
}

/** Implementation of [[PaintShopSolver]] that works for linear time  */
object LinearPaintShopSolver extends PaintShopSolver{

  import BagMerger._

  /** Solution method
    *
    * @param order whole order from all customers
    * @return [[Impossible]], if solution doesn't exist, [[Success]] for solution
    */
  def solve(order: Order): Solution = {
    solveRec(prepareArgs(order), findUnary(order), Success(), order.colorsNum)
  }

  /** Recursive function for solving task
    *
    * @param leftClauses unprocessed clauses
    * @param unaryClauses set of initially found unary clauses
    * @param solution accumulator for solution
    * @param colorNum amount of colors
    * @return result, [[Impossible]], if solution doesn't exist, [[Success]] for solution
    */
  @tailrec
  private def solveRec(leftClauses: Map[Color, Seq[OrderItem]],
                       unaryClauses: Seq[OrderItem],
                       solution: Success,
                       colorNum: Int): Solution = unaryClauses match {
    case Seq() => fillSolutionWithGlossy(solution, colorNum)
    case xs +: tail =>
      val unaryClause = xs.clauses.head
      if (clauseViolates(solution, unaryClause)) {
        Impossible
      } else {
        val (newLeftClauses, newUnary) = processUnaryClause(leftClauses, unaryClause)
        solveRec(newLeftClauses, tail ++ newUnary, solution + (unaryClause.color -> unaryClause.colorType), colorNum)
      }
  }

  /** Function to process unaryClause. Unary clause has to be satisfied anyway. Also clauses with the same colors should be simplified
    *
    * @param leftClauses unprocessed clauses
    * @param unaryClause current unary clause
    * @return tuple of (left unprocessed clauses, new unary clauses)
    */
  private def processUnaryClause(leftClauses: Map[Color, Seq[OrderItem]], unaryClause: Clause): (Map[Color, Seq[OrderItem]], Seq[OrderItem]) = {
    //simplify clauses
    val orderItemsToSimplify = leftClauses.getOrElse(unaryClause.color, Seq())
    simplifyOrderItems(orderItemsToSimplify, Seq(), leftClauses, unaryClause)
  }

  /** Recursive function for simplifying [[OrderItem]]s due to conforming current unary clause
    *
    * @param orderItemsToSimplify [[OrderItem]]s that contains same colors as in current unary clause
    * @param unaryClauses accumulator for new unary clauses that can appear during simp[lification
    * @param leftClauses unprocessed clauses
    * @param unaryClause current unary [[Clause]]
    * @return tuple of (left unprocessed clauses, new unary clauses)
    */
  @tailrec
  private def simplifyOrderItems(orderItemsToSimplify: Seq[OrderItem], unaryClauses: Seq[OrderItem], leftClauses: Map[Color, Seq[OrderItem]],
                                 unaryClause: Clause): (Map[Color, Seq[OrderItem]], Seq[OrderItem]) = orderItemsToSimplify match {
    case Seq() => (leftClauses, unaryClauses)
    case orderItemToSimplify +: tail =>
      if (isCorresponding(orderItemToSimplify, unaryClause)) {
        //remove satisfied OrderItem
        simplifyOrderItems(tail, unaryClauses, removeOrderItem(leftClauses, orderItemToSimplify), unaryClause)
      } else {
        //simplify -> remove clause from OrderItem
        val simplifiedOrderItem = OrderItem(orderItemToSimplify.clauses.filterNot(_.color == unaryClause.color))
        //substitute for simplified
        val newUnaryClauses = unaryClauses ++ (if (simplifiedOrderItem.isUnary) Seq(simplifiedOrderItem) else Seq())
        val newLeftClauses = addOrderItem(removeOrderItem(leftClauses, orderItemToSimplify), simplifiedOrderItem)
        simplifyOrderItems(tail, newUnaryClauses, newLeftClauses, unaryClause)
      }
  }

  /** Check if clause violates current solution
    *
    * @param solution not finished solution
    * @param unaryClause clause to check
    * @return true, if clause violates solution, otherwise false
    */
  def clauseViolates(solution: Success, unaryClause: Clause): Boolean = {
    solution.result.getOrElse(unaryClause.color, unaryClause.colorType) != unaryClause.colorType
  }

  /** if there is no unary clause due to HORNSAT mark all unprocessed colors as Glossy
    *
    * @param solution not completed solution
    * @param colorNum amount of colors
    * @return solution where absent colors filled with default [[ColorType.Glossy]] type
    */
  private def fillSolutionWithGlossy(solution: Success, colorNum: Color): Success = {
    Success.allGlossy(colorNum) ++ solution
  }

  /** Remove [[OrderItem]] from [[Map]] structure of unprocessed clauses from corresponding buckets
    *
    * @param leftClauses structure of not processed clauses
    * @param orderItemToRemove [[OrderItem]] to remove
    * @return new structure without processed [[OrderItem]]
    */
  private def removeOrderItem(leftClauses: Map[Color, Seq[OrderItem]], orderItemToRemove: OrderItem): Map[Color, Seq[OrderItem]] = {
    var result = leftClauses
    orderItemToRemove.clauses.foreach { clause =>
      result += clause.color -> result.getOrElse(clause.color, Seq()).filterNot(_ == orderItemToRemove)
    }
    result
  }

  /** Add [[OrderItem]] to [[Map]] structure to corresponding buckets
    *
    * @param leftClauses structure of not processed clauses
    * @param orderItemToAdd [[OrderItem]] to add
    * @return new structure with added [[OrderItem]]
    */
  private def addOrderItem(leftClauses: Map[Color, Seq[OrderItem]], orderItemToAdd: OrderItem): Map[Color, Seq[OrderItem]] = {
    var result = leftClauses
    orderItemToAdd.clauses.foreach { clause =>
      result += clause.color -> (result.getOrElse(clause.color, Seq()) :+ orderItemToAdd)
    }
    result
  }

  /** Determine if [[OrderItem]] is connected with [[Clause]]
    *
    * @param orderItem order item
    * @param clause clause
    * @return [[Boolean]] value, true - if [[OrderItem]] contains color with the same [[ColorType]],
    *        false - if [[OrderItem]] contains color with other [[ColorType]]
    */
  private def isCorresponding(orderItem: OrderItem, clause: Clause): Boolean = {
    orderItem.clauses.find(_.color == clause.color).map(_.colorType == clause.colorType).get
  }

  /** Find unary [[OrderItem]] from [[Order]]
    *
    * @param order input data
    * @return all unary [[OrderItem]]s from initial [[Order]]
    */
  private[paintshop] def findUnary(order: Order): Seq[OrderItem] = {
    order.items.filter(_.isUnary)
  }

  /** Convert input data to special structure to achive linear performance
    *
    * @param order input data [[Order]]
    * @return map of connections between [[Color]] and [[OrderItem]] that contains such color
    */
  private[paintshop] def prepareArgs(order: Order): Map[Color, Seq[OrderItem]] = {
    order.items.foldLeft(Map[Color, Seq[OrderItem]]())((m, orderItem) => {
      m join orderItem.clauses.map(clause => (clause.color, Seq(orderItem))).toMap
    })
  }
}

object Main extends App {

  val filePath = if (args.length > 1 && "--file" == args(0)) Some(args(1)) else None

  val scanner = if (filePath.isEmpty) {
    println("Please enter your data")
    Scanner.stdIn
  } else {
    File(filePath.get).newScanner
  }
  val input = FileUtil.read(scanner)
  val result = FileUtil.write(input.map(LinearPaintShopSolver.solve))
  println(result)

}




