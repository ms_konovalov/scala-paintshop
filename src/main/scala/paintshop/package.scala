/** Package object with some helper functions */
package object paintshop {

  /** type alias for color of paint */
  type Color = Int

  trait BagMerger {
    type Bag[K, V] = Map[K, Seq[V]]

    def mergeBags[K, V](m1: Bag[K, V], m2: Bag[K, V]): Bag[K, V] =
      (m1.keys ++ m2.keys).map(k => (k, m1.getOrElse(k, Seq()) ++ m2.getOrElse(k, Seq()))).toMap
  }

  /** Implicit enricher for Map to join 2 maps together */
  object BagMerger {

    implicit class RichMapToSeq[K, V](bag: Map[K, Seq[V]]) extends BagMerger {
      def join[B >: V](that: Map[K, Seq[B]]): Map[K, Seq[B]] = mergeBags(this.bag, that)
    }

  }

  /** Utility object for file reading and result serializing */
  object FileUtil {

    import better.files._

    /** Reads input data from file
      *
      * @param scanner scanner that reads input data
      * @return set of test cases
      */
    def read(scanner: Scanner): Array[Order] = {
      (1 to scanner.next[Int]).map { i =>
        val colorsNum = scanner.next[Int]
        val customersNum = scanner.next[Int]
        Order((1 to customersNum).map { j =>
          val itemsNum = scanner.next[Int]
          OrderItem((1 to itemsNum).map { k =>
            val color: Color = scanner.next[Int]
            val colorT = scanner.next[Int]
            val colorType: ColorType.Value = ColorType.apply(colorT)
            Clause(color, colorType)
          })
        }, colorsNum)
      }.toArray
    }

    /** Writes set of results to string
      *
      * @param result array of [[Solution]]s
      * @return [[String]] representation
      */
    def write(result: Array[Solution]): String = {
      result.zipWithIndex.map { case (x, i) => s"Case #${i + 1}: $x"}.mkString("\n")
    }
  }
}
