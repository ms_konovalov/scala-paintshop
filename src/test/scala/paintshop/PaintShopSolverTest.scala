package paintshop

import org.scalatest.prop.TableDrivenPropertyChecks
import org.scalatest.{FreeSpec, Matchers}
import paintshop.ColorType.{Glossy, Matte}

import scala.collection.Map

class PaintShopSolverTest extends FreeSpec with Matchers with TableDrivenPropertyChecks {

  val argConversionTable = Table(
    ( "arg",
      "expected"),
    ( Order(0),
      Map[Color, Seq[OrderItem]]()),
    ( Order(0, OrdItem()),
      Map[Color, Seq[OrderItem]]()),
    ( Order(0, OrdItem(), OrdItem(), OrdItem()),
      Map[Color, Seq[OrderItem]]()),
    ( Order(1, OrdItem((1, Matte)), OrdItem((1, Glossy)), OrdItem()),
      Map[Color, Seq[OrderItem]](
        1 -> Seq(OrdItem((1, Matte)), OrdItem((1, Glossy))))),
    ( Order(3, OrdItem((1, Matte), (2, Glossy)), OrdItem((1, Glossy), (3, Glossy)), OrdItem((3, Matte))),
      Map[Color, Seq[OrderItem]](
        1 -> Seq(OrdItem((1, Matte), (2, Glossy)), OrdItem((1, Glossy), (3, Glossy))),
        2 -> Seq(OrdItem((1, Matte), (2, Glossy))),
        3 -> Seq(OrdItem((1, Glossy), (3, Glossy)), OrdItem((3, Matte))))),
    ( Order(3, OrdItem((1, Matte), (2, Glossy)), OrdItem((1, Glossy), (3, Glossy)), OrdItem((3, Matte)), OrdItem((3, Matte))),
      Map[Color, Seq[OrderItem]](
        1 -> Seq(OrdItem((1, Matte), (2, Glossy)), OrdItem((1, Glossy), (3, Glossy))),
        2 -> Seq(OrdItem((1, Matte), (2, Glossy))),
        3 -> Seq(OrdItem((1, Glossy), (3, Glossy)), OrdItem((3, Matte)), OrdItem((3, Matte)))))
  )

  "LinearPaintShopSolver should convert input data into special structure" in {
    forAll(argConversionTable) { (arg, expected) =>
      val result = LinearPaintShopSolver.prepareArgs(arg)
      result shouldBe expected
    }
  }

  val unaryClausesTable = Table(
    ( "input",
      "expected"),
    ( Order(0),
      Seq[OrderItem]()),
    ( Order(0, OrdItem()),
      Seq[OrderItem]()),
    ( Order(0, OrdItem(), OrdItem(), OrdItem()),
      Seq[OrderItem]()),
    ( Order(1, OrdItem((1, Matte)), OrdItem((1, Glossy)), OrdItem()),
      Seq[OrderItem](OrdItem((1, Matte)), OrdItem((1, Glossy)))),
    ( Order(3, OrdItem((1, Matte), (2, Glossy)), OrdItem((1, Glossy), (3, Glossy)), OrdItem((3, Matte))),
      Seq[OrderItem](OrdItem((3, Matte))))
  )

  "LinearPaintShopSolver should find unary clauses" in {
    forAll(unaryClausesTable) { (input, expected) =>
      val result = LinearPaintShopSolver.findUnary(input)
      result shouldBe expected
    }
  }

  val solutionTable = Table(
    ( "input",
      "expected"),
    ( Order(1, OrdItem((1, Matte))),
      Success(1 -> Matte)),
    ( Order(1, OrdItem((1, Glossy))),
      Success(1 -> Glossy)),
    ( Order(1,
      OrdItem((1, Glossy)),
      OrdItem((1, Glossy))),
      Success(1 -> Glossy)),
    ( Order(1,
      OrdItem((1, Glossy)),
      OrdItem((1, Matte))),
      Impossible),
    ( Order(2,
      OrdItem((1, Glossy)),
      OrdItem((2, Matte))),
      Success(1 -> Glossy, 2 -> Matte)),
    ( Order(2,
      OrdItem((1, Glossy), (2, Matte)),
      OrdItem((2, Matte))),
      Success(1 -> Glossy, 2 -> Matte)),
    ( Order(2,
      OrdItem((1, Glossy), (2, Matte)),
      OrdItem((2, Matte), (1, Glossy))),
      Success(1 -> Glossy, 2 -> Glossy)),
    ( Order(5,
      OrdItem((1, Matte)),
      OrdItem((2, Glossy), (1, Glossy)),
      OrdItem((5, Glossy))),
      Success(1 -> Matte, 2 -> Glossy, 3 -> Glossy, 4 -> Glossy, 5 -> Glossy)),
    ( Order(5,
      OrdItem((1, Matte)),
      OrdItem((1, Matte)),
      OrdItem((1, Matte)),
      OrdItem((2, Glossy), (1, Glossy)),
      OrdItem((2, Glossy), (1, Glossy)),
      OrdItem((2, Glossy), (1, Glossy)),
      OrdItem((5, Glossy))),
      Success(1 -> Matte, 2 -> Glossy, 3 -> Glossy, 4 -> Glossy, 5 -> Glossy)),
    ( Order(10,
      OrdItem((1, Matte), (2, Glossy)),
      OrdItem((2, Matte), (3, Glossy)),
      OrdItem((3, Matte), (4, Glossy)),
      OrdItem((4, Matte), (5, Glossy)),
      OrdItem((5, Matte), (6, Glossy)),
      OrdItem((6, Matte), (7, Glossy)),
      OrdItem((7, Matte), (8, Glossy)),
      OrdItem((8, Matte), (9, Glossy)),
      OrdItem((9, Matte), (10, Glossy)),
      OrdItem((10, Matte))),
      Success(1 -> Matte, 2 -> Matte, 3 -> Matte, 4 -> Matte, 5 -> Matte, 6-> Matte, 7 -> Matte, 8 -> Matte, 9 -> Matte, 10 -> Matte))

  )

  "LinearPaintShopSolver should solve task successfully" in {
    forAll(solutionTable) { (input, expected) =>
      val result = LinearPaintShopSolver.solve(input)
      result shouldBe expected
    }
  }
}
