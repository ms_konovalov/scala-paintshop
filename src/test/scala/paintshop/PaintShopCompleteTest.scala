package paintshop

import better.files._
import org.scalatest.prop.TableDrivenPropertyChecks
import org.scalatest.{FreeSpec, Matchers}

class PaintShopCompleteTest extends FreeSpec with Matchers with TableDrivenPropertyChecks {

  val files = Table(
    ( "in",
      "out"),
    ("/in1.txt",
      "/out1.txt")
  )

  "should read data from file, solve task and print result" in {
    forAll(files) { (in, out) =>
      val orders = FileUtil.read(File(getClass.getResource(in).toURI).newScanner)
      val expected = File(getClass.getResource(out).toURI)
      val result = FileUtil.write(orders.map(LinearPaintShopSolver.solve))
      println(result)
      result shouldBe expected.contentAsString
    }
  }
}
